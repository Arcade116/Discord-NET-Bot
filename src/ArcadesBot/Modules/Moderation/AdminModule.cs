﻿using ArcadesBot.CommandExtensions.Preconditions;
using ArcadesBot.Interactive;
using Discord;
using Discord.Commands;

namespace ArcadesBot.Modules.Moderation
{
    [Name("Admin"), RequirePermission(AccessLevel.Administrator), RequireBotPermission(ChannelPermission.SendMessages)]
    [Summary("Several Admin commands")]
    public class AdminModule : InteractiveBase
    {

    }
}
