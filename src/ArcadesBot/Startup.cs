﻿using ChessDotNet;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using ArcadesBot.CommandExtensions.TypeReaders;
using ArcadesBot.Data;
using ArcadesBot.Handlers;
using ArcadesBot.Helpers;
using ArcadesBot.Interactive;
using ArcadesBot.Models;
using ArcadesBot.Services;
using ArcadesBot.Services.Chess;
using ArcadesBot.Utility;

namespace ArcadesBot
{
    public class Startup
    {
        public async Task<IServiceProvider> ConfigureServicesAsync()
        {
            var services = new ServiceCollection()
                .AddDbContext<GuildDatabase>(ServiceLifetime.Transient)
                .AddDbContext<ChessDatabase>(ServiceLifetime.Transient)
                .AddSingleton<DatabaseHandler>()
                .AddSingleton<ChessHandler>()
                .AddSingleton<GuildHandler>()
                .AddSingleton<ChessStatsHandler>()
                .AddSingleton<SchedulerService>()
                .AddSingleton<CommandManager>()
                .AddSingleton<RoslynManager>()
                .AddSingleton<Random>()
                .AddSingleton<HttpClient>()
                .AddSingleton<AssetService>()
                .AddSingleton<ChessService>()
                .AddSingleton<WebhookService>()
                .AddSingleton<InteractiveService>()
                .AddSingleton<GuildHelper>()
                .AddSingleton<WebhookService>()
                .AddSingleton<ChessGame>();

            // Discord
            await LoadDiscord(services);

            // Google
            await LoadGoogle(services);
            
            var provider = new DefaultServiceProviderFactory().CreateServiceProvider(services);
            return provider;
        }

        private Task LoadDiscord(IServiceCollection services)
        {
            var discord = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Verbose,
                MessageCacheSize = 1000,
            });

            var commands = new CommandService(new CommandServiceConfig
            {
                LogLevel = LogSeverity.Verbose,
                DefaultRunMode = RunMode.Async,
                ThrowOnError = false
            });
            
            discord.Log += OnLog;
            commands.Log += OnLog;


            commands.AddTypeReader<TagModel>(new TagModelTypeReader());

            services.AddSingleton(discord);
            services.AddSingleton(commands);
            return Task.CompletedTask;
        }

        public Task LoadGoogle(IServiceCollection services)
        {
            var provider = new DefaultServiceProviderFactory().CreateServiceProvider(services);

            var databaseHandler = provider.GetService<DatabaseHandler>();

            var youtube = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = databaseHandler.Config.ApiKeys["Google"],
                MaxUrlLength = 256
            });
            services.AddSingleton(youtube);
            return Task.CompletedTask;
        }

        private Task OnLog(LogMessage msg)
        {
            PrettyConsole.Log(msg.Severity, msg.Source, msg.Exception?.ToString() ?? msg.Message);
            return Task.CompletedTask;
        }
            
    }
}
