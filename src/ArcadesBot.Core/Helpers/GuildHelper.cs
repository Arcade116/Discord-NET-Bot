﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArcadesBot.Handlers;
using ArcadesBot.Models;
using Discord;
using Discord.WebSocket;

namespace ArcadesBot.Helpers
{
    public class GuildHelper
    {
        public GuildHelper(GuildHandler guildHandler, DiscordSocketClient client, DatabaseHandler database)
        {
            _client = client;
            _guildHandler = guildHandler;
            _database = database;
        }

        private GuildHandler _guildHandler { get; }
        private DiscordSocketClient _client { get; }
        private DatabaseHandler _database { get; }

        public async Task<UserProfile> GetProfileAsync(ulong guildId, ulong userId)
        {
            var guild = _guildHandler.GetGuild(guildId);
            if (guild.ProfilesDict.ContainsKey(userId))
                return guild.ProfilesDict[userId];
            guild.ProfilesDict.Add(userId, new UserProfile());
            await _database.UpdateGuildAsync(guild);
            return guild.ProfilesDict[userId];
        }
        public async Task SaveProfileAsync(ulong guildId, ulong userId, UserProfile profile)
        {
            var guild = _guildHandler.GetGuild(guildId);
            guild.ProfilesDict[userId] = profile;
            await _database.UpdateGuildAsync(guild);
        }
    }
}