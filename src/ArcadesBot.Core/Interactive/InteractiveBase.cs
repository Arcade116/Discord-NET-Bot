﻿using ArcadesBot.Common;
using ArcadesBot.Interactive.Criteria;
using ArcadesBot.Interactive.Paginator;
using ArcadesBot.Interactive.Results;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ArcadesBot.Interactive
{

    public class InteractiveBase : Base
    {
        public InteractiveService Interactive { get; set; }

        public Task<SocketMessage> NextMessage(ICriterion<SocketMessage> criterion, TimeSpan? timeout = null)
            => Interactive.NextMessageAsync(Context, criterion, timeout);
        public Task<SocketMessage> NextMessage(bool fromSourceUser = true, bool inSourceChannel = true, TimeSpan? timeout = null) 
            => Interactive.NextMessage(Context, fromSourceUser, inSourceChannel, timeout);



        public Task<IUserMessage> ReplyAndDelete(string content, bool isTts = false, Embed embed = null, TimeSpan? timeout = null, RequestOptions options = null)
            => Interactive.ReplyAndDeleteAsync(Context, content, isTts, embed, timeout, options);

        public Task<IUserMessage> PagedReply(IEnumerable<object> pages, bool fromSourceUser = true)
        {
            var pager = new PaginatedMessage
            {
                Pages = pages
            };
            return PagedReply(pager, fromSourceUser);
        }

        public Task<IUserMessage> PagedReply(PaginatedMessage pager, bool fromSourceUser = true)
        {
            var criterion = new Criteria<SocketReaction>();
            if (fromSourceUser)
                criterion.AddCriterion(new EnsureReactionFromSourceUserCriterion());
            return PagedReply(pager, criterion);
        }

        public Task<IUserMessage> PagedReply(PaginatedMessage pager, ICriterion<SocketReaction> criterion)
            => Interactive.SendPaginatedMessageAsync(Context, pager, criterion);

        public RuntimeResult Ok(string reason = null) => new OkResult(reason);
    }
}