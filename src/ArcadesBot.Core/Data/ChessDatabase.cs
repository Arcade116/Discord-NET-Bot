﻿using ArcadesBot.Models.Chess;
using Microsoft.EntityFrameworkCore;

namespace ArcadesBot.Data
{
    public class ChessDatabase : DbContext
    {
        public DbSet<ChessChallengeModel> ChessChallenge { get; set; }
        public DbSet<ChessMatchModel> ChessMatch { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=D:\\database.db");
        }
    }
}