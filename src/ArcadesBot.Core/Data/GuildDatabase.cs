﻿using ArcadesBot.Models;
using ArcadesBot.Models.Chess;
using Microsoft.EntityFrameworkCore;

namespace ArcadesBot.Data
{
    public class GuildDatabase : DbContext
    {
        public DbSet<GuildModel> Guilds { get; set; }
        //public DbSet<ChessMatchStatsModel> ChessMatchStats { get; set; }

        public GuildDatabase() 
            => Database.EnsureCreated();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=D:\\database.db");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<GuildModel>(b =>
            {
                b.ToTable("Guilds");
            });
        }
    }
}