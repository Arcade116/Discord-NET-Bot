﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using ArcadesBot.Models.Chess;
using ArcadesBot.Utility;
using Discord;
using Microsoft.EntityFrameworkCore;

namespace ArcadesBot.Handlers
{
    public class ChessHandler
    {
        public ChessHandler(DatabaseHandler databaseHandler) 
            => _database = databaseHandler;

        private DatabaseHandler _database { get; }

        public bool CheckPlayerInMatch(ulong guildId, ulong invokerId) 
            => _database.Matches.Any(x => x.GuildId == guildId && (x.ChallengeeId == invokerId || x.ChallengerId == invokerId) && x.Winner == 1);
        public ChessMatchModel GetMatch(ulong id)
            => _database.GetChessGame(id);
        public ChessMatchModel GetMatch(ulong guildId, ulong channelId, ulong invokerId)
            => _database.Matches.FirstOrDefault(x => x.GuildId == guildId && x.ChannelId == channelId && (x.ChallengeeId == invokerId || x.ChallengerId == invokerId) && x.Winner == 1);
        public ChessChallengeModel GetChallenge(ulong id)
            => _database.GetChallenge(id);
        public ChessChallengeModel GetChallenge(ulong guildId, ulong channelId, ulong invokerId, bool overRide = false)
        {
            if (overRide)
                return _database.Challenges.Where(x => x.GuildId == guildId && x.ChannelId == channelId && x.ChallengeeId == invokerId).OrderByDescending(x => x.TimeoutDate).FirstOrDefault();
            return _database.Challenges.Where(x => x.GuildId == guildId && x.ChannelId == channelId &&
                            (x.ChallengeeId == invokerId || x.ChallengerId == invokerId)).OrderByDescending(x => x.TimeoutDate).FirstOrDefault();
        }
        public async Task AddMatchAsync(ulong guildId, ulong channelId, ulong challenger, ulong challengee, string whiteAvatarUrl,
            string blackAvatarUrl)
        {
            var data = new ChessMatchModel
            {
                ChallengerId = challenger,
                ChallengeeId = challengee,
                ChannelId = channelId,
                GuildId = guildId,
                WhiteAvatarUrl = whiteAvatarUrl,
                BlackAvatarUrl = blackAvatarUrl,
                HistoryList = new List<ChessMoveModel>()
            };
            await _database.CreateMatchAsync(data);
        }

        public async Task UpdateChallengeAsync(ChessChallengeModel chessChallenge)
        {
            if (chessChallenge == null)
                return;
            await _database.UpdateChallengeAsync(chessChallenge);
        }

        public async Task RemoveChallengeAsync(ChessChallengeModel chessChallenge)
        {
            if (chessChallenge == null)
                return;
            await _database.DeleteChallengeAsync(chessChallenge.Id);
        }

        public ChessChallengeModel CreateChallenge(ulong guildId, ulong channelId, IUser challenger, IUser challengee)
        {
            var challenge = new ChessChallengeModel
            {
                ChallengerId = challenger.Id,
                ChallengeeId = challengee.Id,
                ChannelId = channelId,
                GuildId = guildId,
                Accepted = false,
                DateCreated = DateTime.Now,
                TimeoutDate = DateTime.Now.AddMinutes(1)
            };
            _database.CreateChallenge(challenge);
            return GetChallenge(guildId, channelId, challenger.Id);
        }

        public async Task UpdateChessGameAsync(ChessMatchModel matchData)
        {
            if (matchData.Status == Cause.Stalemate)
                matchData.Winner = 0;
            if (matchData.Winner != 1)
            {
                matchData.Stat = new ChessMatchStatsModel
                {
                    GuildId = matchData.GuildId,
                    ParticipantsArray = new[] { matchData.ChallengerId, matchData.ChallengeeId },
                    CreatedBy = matchData.ChallengerId,
                    Winner = matchData.Winner,
                    EndBy = matchData.Status,
                    MoveCount = (ulong)matchData.HistoryList.Count
                };
                matchData.HistoryList = new List<ChessMoveModel>();
            }
            await _database.UpdateMatchAsync(matchData);
        }
        public async Task<ChessMatchModel> AcceptChallengeAsync(ChessChallengeModel challenge, string blackUrl, string whiteUrl)
        {
            await AddMatchAsync(challenge.GuildId, challenge.ChannelId, challenge.ChallengerId,
                challenge.ChallengeeId, whiteUrl, blackUrl);
            await RemoveChallengeAsync(challenge);
            return GetMatch(challenge.GuildId, challenge.ChannelId, challenge.ChallengerId);
        }

        public async Task<ulong> ResignAsync(ulong guildId, ulong channelId, ulong invokerId)
        {
            var chessMatch = GetMatch(guildId, channelId, invokerId);
            //Retrieve the Match again to track it 
            chessMatch = GetMatch(chessMatch.Id);
            chessMatch.Winner = chessMatch.ChallengeeId == invokerId
                ? chessMatch.ChallengerId
                : chessMatch.ChallengeeId;
            chessMatch.Status = Cause.Resign;
            await UpdateChessGameAsync(chessMatch);
            return chessMatch.Winner;
        }
    }
}