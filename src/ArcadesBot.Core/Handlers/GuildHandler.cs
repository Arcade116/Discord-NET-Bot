﻿using System.Threading.Tasks;
using ArcadesBot.Models;

namespace ArcadesBot.Handlers
{
    public class GuildHandler
    {
        public GuildHandler(DatabaseHandler database) 
            => _database = database;

        private DatabaseHandler _database { get; }

        public GuildModel GetGuild(ulong id) 
            => _database.GetGuild(id);

        public Task RemoveGuild(ulong id)
            => _database.DeleteGuildAsync(id);

        public Task AddGuild(ulong id) 
            => _database.CreateGuildAsync(new GuildModel(id));
    }
}