﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ArcadesBot.Data;
using ArcadesBot.Models;
using ArcadesBot.Models.Chess;
using ArcadesBot.Utility;
using Discord;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace ArcadesBot.Handlers
{
    public class DatabaseHandler
    {

        public DatabaseHandler(GuildDatabase guildDatabase, ChessDatabase chessDatabase)
        {
            _guildDatabase = guildDatabase;
            _chessDatabase = chessDatabase;
        }

        private GuildDatabase _guildDatabase { get; }
        private ChessDatabase _chessDatabase { get; }

        public ConfigModel Config
        {
            get
            {
                var dbConfigPath = $"{Directory.GetCurrentDirectory()}/config/config.json";
                if (File.Exists(dbConfigPath))
                    return JsonConvert.DeserializeObject<ConfigModel>(File.ReadAllText(dbConfigPath));
                PrettyConsole.Log(LogSeverity.Info, "Arcade's Bot", "Enter Bot's Token: ");
                var token = Console.ReadLine();
                PrettyConsole.Log(LogSeverity.Info, "Arcade's Bot", "Enter Bot's Prefix: ");
                var prefix = Console.ReadLine();
                var model = new ConfigModel
                {
                    Prefix = prefix.Trim(),
                    Blacklist = new List<ulong>(),
                    ApiKeys = new Dictionary<string, string>
                    {
                        {"Giphy", "dc6zaTOxFJmzC"},
                        {"Google", ""},
                        {"Discord", token.Trim()},
                        {"Imgur", ""}
                    }
                };
                File.WriteAllText(dbConfigPath, JsonConvert.SerializeObject(model, Formatting.Indented));
                return JsonConvert.DeserializeObject<ConfigModel>(File.ReadAllText(dbConfigPath));

            }
        }
        public IQueryable<GuildModel> Guilds
            => _guildDatabase.Guilds.AsNoTracking();

        public IQueryable<ChessChallengeModel> Challenges
            => _chessDatabase.ChessChallenge.AsNoTracking();

        public IQueryable<ChessMatchModel> Matches
            => _chessDatabase.ChessMatch.AsNoTracking();

        #region Guild Methods 
        public GuildModel GetGuild(ulong id)
            => _guildDatabase.Guilds.FirstOrDefault(x => x.Id == id);

        public async Task<GuildModel> CreateGuildAsync(GuildModel data)
        {
            var guild = _guildDatabase.Guilds.FirstOrDefault(x => x.Id == data.Id);
            if (guild != null)
                return guild;
            await _guildDatabase.Guilds.AddAsync(data);
            await _guildDatabase.SaveChangesAsync();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Added Guild with id: {data.Id}.");
            return _guildDatabase.Guilds.FirstOrDefault(x => x.Id == data.Id);
        }
        public async Task<GuildModel> UpdateGuildAsync(GuildModel data)
        {
            _guildDatabase.Guilds.Update(data);
             await _guildDatabase.SaveChangesAsync();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Updated Guild with id: {data.Id}.");
            return _guildDatabase.Guilds.FirstOrDefault(x => x.Id == data.Id);
        }
        public async Task<bool> DeleteGuildAsync(ulong id)
        {
            var guild = _guildDatabase.Guilds.FirstOrDefault(x => x.Id == id);
            if (guild == null)
                return true;
            _guildDatabase.Guilds.Remove(guild);
            if (await _guildDatabase.SaveChangesAsync() == 0)
                return false;
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Deleted Guild with id: {id}.");
            return true;
        }

        #endregion

        #region Challenge Methods 
        public ChessChallengeModel GetChallenge(ulong id)
            => _chessDatabase.ChessChallenge.FirstOrDefault(x => x.Id == id);
        public void CreateChallenge(ChessChallengeModel data)
        {
            _chessDatabase.ChessChallenge.Add(data);
            _chessDatabase.SaveChanges();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Added Challenge with id: {data.Id}.");
        }
        public async Task<ChessChallengeModel> UpdateChallengeAsync(ChessChallengeModel data)
        {
            _chessDatabase.ChessChallenge.Update(data);
            await _chessDatabase.SaveChangesAsync();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Updated Challenge with id: {data.Id}.");
            return _chessDatabase.ChessChallenge.FirstOrDefault(x => x.Id == data.Id);
        }
        public async Task<bool> DeleteChallengeAsync(ulong id)
        {
            var challenge = _chessDatabase.ChessChallenge.FirstOrDefault(x => x.Id == id);
            if (challenge == null)
                return true;
            _chessDatabase.ChessChallenge.Remove(challenge);
            if (await _chessDatabase.SaveChangesAsync() == 0)
                return false;
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Deleted Challenge with id: {id}.");
            return true;
        }

        #endregion

        #region Match Methods 
        public ChessMatchModel GetChessGame(ulong id)
            => _chessDatabase.ChessMatch.FirstOrDefault(x => x.Id == id);

        public async Task CreateMatchAsync(ChessMatchModel data)
        {
            await _chessDatabase.ChessMatch.AddAsync(data);
            await _chessDatabase.SaveChangesAsync();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Added Match with id: {data.Id}.");
        }
        public async Task<ChessMatchModel> UpdateMatchAsync(ChessMatchModel data)
        {
            _chessDatabase.ChessMatch.Update(data);
            await _chessDatabase.SaveChangesAsync();
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Updated Match with id: {data.Id}.");
            return _chessDatabase.ChessMatch.FirstOrDefault(x => x.Id == data.Id);
        }
        public async Task<bool> DeleteMatchAsync(ulong id)
        {
            var challenge = _chessDatabase.ChessMatch.FirstOrDefault(x => x.Id == id);
            if (challenge == null)
                return true;
            _chessDatabase.ChessMatch.Remove(challenge);
            if (await _chessDatabase.SaveChangesAsync() == 0)
                return false;
            PrettyConsole.Log(LogSeverity.Info, "Database", $"Deleted Match with id: {id}.");
            return true;
        }

        #endregion
    }
}