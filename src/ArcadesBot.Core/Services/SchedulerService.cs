﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ArcadesBot.Handlers;
using ArcadesBot.Models.Chess;
using FluentScheduler;

namespace ArcadesBot.Services
{
    public class SchedulerService
    {
        public SchedulerService(DatabaseHandler database)
        {
            _database = database;
        }

        private DatabaseHandler _database { get; }

        public Task Initialize()
        {
            JobManager.Initialize();
            //JobManager.AddJob(async () => await deleteExpiredChallengesAsync(), x => x.ToRunEvery(10).Seconds());
            return Task.CompletedTask;
        }

        private async Task deleteExpiredChallengesAsync()
        {
            var challenges = _database.Challenges
                .Where(x => x.Accepted == false && x.TimeoutDate.AddSeconds(20) < DateTime.Now);

            foreach (var challenge in challenges)
                await _database.DeleteChallengeAsync(challenge.Id);

        }
    }
}