﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ArcadesBot.Common;
using ArcadesBot.Handlers;
using ArcadesBot.Helpers;
using ArcadesBot.Models.Chess;
using ArcadesBot.Utility;
using ChessDotNet;
using Discord;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using File = ChessDotNet.File;
using Image = SixLabors.ImageSharp.Image;

namespace ArcadesBot.Services.Chess
{
    public class ChessService
    {
        public ChessService(AssetService assetService, ChessHandler chessHandler, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _chessHandler = chessHandler;
            _assetService = assetService;
        }

        private AssetService _assetService { get; }
        private ChessHandler _chessHandler { get; }
        private HttpClient _httpClient { get; }

        #region Public Methods

        public ulong WhoseTurn(ChessMatchModel match)
        {
            var game = match.HistoryList.Count == 0
                    ? new ChessGame()
                    : new ChessGame(match.HistoryList.Select(x => x.Move), true);

            return game.WhoseTurn != Player.White ? match.ChallengeeId : match.ChallengerId;
        }

        public async Task<ImageLinkModel> WriteBoardAsync(ulong guildId, ulong channelId, ulong playerId)
        {
            var match = _chessHandler.GetMatch(guildId, channelId, playerId);
            if (match == null)
                throw new ChessException("You are not in a game.");
            var moves = match.HistoryList.Select(x => x.Move);
            var game = moves.Count() != 0
                ? new ChessGame(moves, true)
                : new ChessGame();
            var otherPlayer = game.WhoseTurn == Player.White ? Player.Black : Player.White;
            var linkFromMatch = await drawBoardAsync(match);
            linkFromMatch.IsCheck = game.IsInCheck(otherPlayer);
            linkFromMatch.IsCheckmated = game.IsCheckmated(otherPlayer);
            return linkFromMatch;
        }


        public ChessChallengeModel Challenge(ulong guildId, ulong channelId, IUser player1, IUser player2,
            Action<ChessChallengeModel> onTimeout = null)
        {
            if (player1.Equals(player2))
                throw new ChessException("You can't challenge yourself.");
            if (_chessHandler.CheckPlayerInMatch(guildId, player1.Id))
                throw new ChessException($"{player1.Mention} is currently in a game.");
            if (_chessHandler.CheckPlayerInMatch(guildId, player2.Id))
                throw new ChessException($"{player2.Mention} is currently in a game.");

            var challenge = _chessHandler.CreateChallenge(guildId, channelId, player1, player2);
            removeChallengeAsync(challenge, onTimeout);
            return challenge;
        }

        public ulong Resign(ulong guildId, ulong channelId, IUser player)
            =>  _chessHandler.ResignAsync(guildId, channelId, player.Id).GetAwaiter().GetResult();

        public async Task<ChessMatchModel> AcceptChallengeAsync(CustomCommandContext context, IUser player)
        {
            var challenge = _chessHandler.GetChallenge(context.Guild.Id, context.Channel.Id, player.Id, true);
            if (challenge == null)
                throw new ChessException("No challenge exists for you to accept.");

            var trackedDbChallenge = _chessHandler.GetChallenge(challenge.Id);

            if (_chessHandler.CheckPlayerInMatch(context.Guild.Id, trackedDbChallenge.ChallengeeId))
                throw new ChessException($"{context.Guild.GetUser(trackedDbChallenge.ChallengeeId)} is currently in a game.");

            var challengee = context.Client.GetUser(trackedDbChallenge.ChallengeeId);
            var challenger = context.Client.GetUser(trackedDbChallenge.ChallengerId);

            var blackUrl = challengee.GetAvatarUrl() ?? challengee.GetDefaultAvatarUrl();
            var whiteUrl = challenger.GetAvatarUrl() ?? challenger.GetDefaultAvatarUrl();

            return await _chessHandler.AcceptChallengeAsync(trackedDbChallenge, blackUrl, whiteUrl);
        }

        public async Task<ImageLinkModel> MoveAsync(ulong guildId, ulong channelId, IUser player, string rawMove)
        {
            var rankToRowMap = new Dictionary<int, int>
            {
                {1, 7},
                {2, 6},
                {3, 5},
                {4, 4},
                {5, 3},
                {6, 2},
                {7, 1},
                {8, 0}
            };
            var moveInput = rawMove.Replace(" ", "").ToUpper();
            if (!Regex.IsMatch(moveInput, "^[A-H][1-8][A-H][1-8][Q|N|B|R]?$"))
                throw new ChessException("Error parsing move. Example move: a2a4");
            var match = _chessHandler.GetMatch(guildId, channelId, player.Id);
            if (match == null)
                throw new ChessException("You are not currently in a game");
            //Retrieve the Match again to track it 
            match = _chessHandler.GetMatch(match.Id);

            var moves = match.HistoryList.Select(x => x.Move);
            var game = moves.Count() != 0
                ? new ChessGame(moves, true)
                : new ChessGame();
            var whoseTurn = game.WhoseTurn;
            var otherPlayer = whoseTurn == Player.White ? Player.Black : Player.White;
            if (whoseTurn == Player.White && player.Id != match.ChallengerId ||
                whoseTurn == Player.Black && player.Id != match.ChallengeeId)
                throw new ChessException("It's not your turn.");

            var sourceX = moveInput[0].ToString();
            var sourceY = moveInput[1].ToString();
            var destX = moveInput[2].ToString();
            var destY = moveInput[3].ToString();

            var positionEnumValues = (IEnumerable<File>)Enum.GetValues(typeof(File));

            var enumValues = positionEnumValues.ToList();
            var sourcePositionX = enumValues.Single(x => x.ToString("g") == sourceX);
            var destPositionX = enumValues.Single(x => x.ToString("g") == destX);

            var originalPosition = new Position(sourcePositionX, int.Parse(sourceY));
            var newPosition = new Position(destPositionX, int.Parse(destY));

            var board = game.GetBoard();
            var file = int.Parse(sourcePositionX.ToString("d"));
            var column = rankToRowMap[int.Parse(sourceY)];
            if (board[column][file] == null)
                throw new ChessException("Invalid move.");
            var pieceChar = board[column][file].GetFenCharacter();
            var isPawn = pieceChar.ToString().ToLower() == "p";
            char? promotion;
            if (destY != "1" && destY != "8" || !isPawn)
                promotion = null;
            else
                promotion = moveInput[4].ToString().ToLower()[0];
            var move = new Move(originalPosition, newPosition, whoseTurn, promotion);

            if (!game.IsValidMove(move))
                throw new ChessException("Invalid move.");
            var chessMove = new ChessMoveModel
            {
                Move = move,
                MovedfenChar = pieceChar,
                MoveDate = DateTime.Now,
                Player = whoseTurn
            };
            game.MakeMove(move, true);
            match.HistoryList.Add(chessMove);

            var endCause = Cause.OnGoing;
            if (game.IsStalemated(otherPlayer))
                endCause = Cause.Stalemate;
            else if (game.IsCheckmated(otherPlayer))
                endCause = Cause.Checkmate;

            match.Status = endCause;
            match.Winner = endCause == Cause.Checkmate ? player.Id : 1;

            var imageLinkValues = await drawBoardAsync(match);

            imageLinkValues.Status = match.Status;
            imageLinkValues.Winner = match.Winner;

            await _chessHandler.UpdateChessGameAsync(match);
            return await Task.FromResult(imageLinkValues);
        }

        #endregion

        #region Private Methods

        private void DrawImage(IImageProcessingContext<Rgba32> processor, string name, int x, int y)
        {
            var image = Image.Load(_assetService.GetImagePath("Chess", $"{name}.png"));
            image.Mutate(img => img.Resize(new Size(50, 50)));
            processor.DrawImage(new GraphicsOptions(), image, new Point(x * 50 + 117, y * 50 + 19));
        }

        private async Task<ImageLinkModel> drawBoardAsync(ChessMatchModel match)
        {
            await Task.Run(async () =>
            {
                var board = Image.Load(_assetService.GetImagePath("Chess", "board.png"));

                var whiteAvatarData = Image.Load(await _httpClient.GetByteArrayAsync(match.WhiteAvatarUrl));
                whiteAvatarData.Mutate(img => img.Resize(new Size(50, 50)));
                var blackAvatarData = Image.Load(await _httpClient.GetByteArrayAsync(match.BlackAvatarUrl));
                blackAvatarData.Mutate(img => img.Resize(new Size(50, 50)));
                var turnIndicator = Image.Load(_assetService.GetImagePath("Chess", "turn_indicator.png"));
                turnIndicator.Mutate(img => img.Resize(new Size(56, 56)));

                var moves = match.HistoryList.Select(x => x.Move).ToList();

                var game = moves.Count != 0 ? new ChessGame(moves, true) : new ChessGame();

                var boardPieces = game.GetBoard();

                var lastMove = match.HistoryList.OrderByDescending(x => x.MoveDate).FirstOrDefault();
                var rankToRowMap = new Dictionary<int, int>
                {
                    {1, 7},
                    {2, 6},
                    {3, 5},
                    {4, 4},
                    {5, 3},
                    {6, 2},
                    {7, 1},
                    {8, 0}
                };
                var turnIndicatorPoint = game.WhoseTurn != Player.Black ? new Point(538, 367) : new Point(40, 15);
                board.Mutate(processor =>
                {
                    #region Mutating the board

                    for (var x = 0; x < boardPieces.Length; ++x)
                    for (var y = 0; y < boardPieces[x].Length; ++y)
                    {
                        if (lastMove != null && (lastMove.Move.OriginalPosition.File == (File) x &&
                                                 rankToRowMap[lastMove.Move.OriginalPosition.Rank] == y
                                                 || lastMove.Move.NewPosition.File == (File) x &&
                                                 rankToRowMap[lastMove.Move.NewPosition.Rank] == y))
                            DrawImage(processor, "yellow_square", x, y);
                        var piece = boardPieces[y][x];
                        if (piece == null)
                                continue;
                        if (piece.GetFenCharacter().ToString().ToUpper() == "K" && game.IsInCheck(piece.Owner))
                            DrawImage(processor, "red_square", x, y);
                        var str = "white";
                        if (new[] {'r', 'n', 'b', 'q', 'k', 'p'}.Contains(piece.GetFenCharacter()))
                            str = "black";
                        DrawImage(processor, $"{str}_{piece.GetFenCharacter()}", x, y);
                    }

                    var blackPawnCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'p' && x.Owner == Player.Black && !x.IsPromotionResult);
                    var whitePawnCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'P' && x.Owner == Player.White && !x.IsPromotionResult);
                    var blackRookCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'r' && x.Owner == Player.Black && !x.IsPromotionResult);
                    var whiteRookCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'R' && x.Owner == Player.White && !x.IsPromotionResult);
                    var blackKnightCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'n' && x.Owner == Player.Black && !x.IsPromotionResult);
                    var whiteKnightCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'N' && x.Owner == Player.White && !x.IsPromotionResult);
                    var blackBishopCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'b' && x.Owner == Player.Black && !x.IsPromotionResult);
                    var whiteBishopCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'B' && x.Owner == Player.White && !x.IsPromotionResult);
                    var blackQueenCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'q' && x.Owner == Player.Black && !x.IsPromotionResult);
                    var whiteQueenCount = game.PiecesOnBoard.Count(x =>
                        x.GetFenCharacter() == 'Q' && x.Owner == Player.White && !x.IsPromotionResult);
                    var row = 1;

                    var blackPawn = Image.Load(_assetService.GetImagePath("Chess", "black_p.png"));
                    blackPawn.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 8; index > blackPawnCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), blackPawn, new Point(533, 16 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), blackPawn, new Point(566, 16 + row * 30));
                            ++row;
                        }
                    }

                    row = 8;

                    var whitePawn = Image.Load(_assetService.GetImagePath("Chess", "white_P.png"));
                    whitePawn.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 8; index > whitePawnCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), whitePawn, new Point(20, 125 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), whitePawn, new Point(53, 125 + row * 30));
                            --row;
                        }
                    }

                    row = 5;

                    var blackRook = Image.Load(_assetService.GetImagePath("Chess", "black_r.png"));
                    blackRook.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 2; index > blackRookCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), blackRook, new Point(533, 16 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), blackRook, new Point(566, 16 + row * 30));
                            ++row;
                        }
                    }

                    row = 4;

                    var whiteRook = Image.Load(_assetService.GetImagePath("Chess", "white_R.png"));
                    whiteRook.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 2; index > whiteRookCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteRook, new Point(20, 125 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteRook, new Point(53, 125 + row * 30));
                            --row;
                        }
                    }

                    row = 6;

                    var blackKnight = Image.Load(_assetService.GetImagePath("Chess", "black_n.png"));
                    blackKnight.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 2; index > blackKnightCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), blackKnight, new Point(533, 16 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), blackKnight, new Point(566, 16 + row * 30));
                            ++row;
                        }
                    }

                    row = 3;

                    var whiteKnight = Image.Load(_assetService.GetImagePath("Chess", "white_N.png"));
                    whiteKnight.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var i = 2; i > whiteKnightCount; --i)
                    {
                        if (i % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteKnight, new Point(20, 125 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteKnight, new Point(53, 125 + row * 30));
                            --row;
                        }
                    }

                    row = 7;

                    var blackBishop = Image.Load(_assetService.GetImagePath("Chess", "black_b.png"));
                    blackBishop.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 2; index > blackBishopCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), blackBishop, new Point(533, 16 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), blackBishop, new Point(566, 16 + row * 30));
                            ++row;
                        }
                    }

                    row = 2;

                    var whiteBishop = Image.Load(_assetService.GetImagePath("Chess", "white_B.png"));
                    whiteBishop.Mutate(img => img.Resize(new Size(30, 30)));
                    for (var index = 2; index > whiteBishopCount; --index)
                    {
                        if (index % 2 == 0)
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteBishop, new Point(20, 125 + row * 30));
                        }
                        else
                        {
                            processor.DrawImage(new GraphicsOptions(), whiteBishop, new Point(53, 125 + row * 30));
                            --row;
                        }
                    }

                    row = 8;

                    var blackQueen = Image.Load(_assetService.GetImagePath("Chess", "black_q.png"));
                    blackQueen.Mutate(img => img.Resize(new Size(30 ,30)));
                    if (blackQueenCount == 0)
                        processor.DrawImage(new GraphicsOptions(), blackQueen,  new Point(533, 16 + row * 30));
                    row = 1;

                    var whiteQueen = Image.Load(_assetService.GetImagePath("Chess", "white_Q.png"));
                    whiteQueen.Mutate(img => img.Resize(new Size(30, 30)));
                    if (whiteQueenCount == 0)
                        processor.DrawImage(new GraphicsOptions(), whiteQueen, new Point(20, 125 + row * 30));

                    processor.DrawImage(new GraphicsOptions(), turnIndicator, turnIndicatorPoint);
                    processor.DrawImage(new GraphicsOptions(), whiteAvatarData, new Point(541, 370));
                    processor.DrawImage(new GraphicsOptions(), blackAvatarData, new Point(43, 18));

                    var table = Image.Load(_assetService.GetImagePath("Chess", "table.png"));
                    table.Mutate(img => img.Resize(new Size(108, 148)));
                    processor.DrawImage(new GraphicsOptions(), table, new Point(596, 18));

                    if (!SystemFonts.TryFind("Arial", out var font))
                    {
                        PrettyConsole.Log(LogSeverity.Critical, "Image drawing", "Cannot find font 'Arial' install Arial as a font and try again");
                        Console.ReadLine();
                        Environment.Exit(0);
                    }

                    processor.DrawText("Last 5 moves", font.CreateFont(12, FontStyle.Bold), Rgba32.Black, new PointF(616, 19));
                    processor.DrawText("White", font.CreateFont(12, FontStyle.Bold), Rgba32.Black, new PointF(606, 42));
                    processor.DrawText("Black", font.CreateFont(12, FontStyle.Bold), Rgba32.Black, new PointF(660, 42));
                    var blackMoves = match.HistoryList.OrderByDescending(x => x.MoveDate).Where(x => x.Player == Player.Black).ToList();
                    var whiteMoves = match.HistoryList.OrderByDescending(x => x.MoveDate).Where(x => x.Player == Player.White).ToList();

                    for (var i = 0; i < blackMoves.Count && i < 5; i++)
                    {
                        var safeguard = i;
                        var player = blackMoves[i].Player.ToString().ToLower();
                        var file = blackMoves[i].Move.NewPosition.File;
                        var rank = blackMoves[i].Move.NewPosition.Rank;
                        var fenChar = blackMoves[i].MovedfenChar;
                        var image = Image.Load(_assetService.GetImagePath("Chess", $"{player}_{fenChar}.png"));
                        image.Mutate(img => img.Resize(new Size(12, 12)));
                        if (blackMoves.Count != whiteMoves.Count)
                            safeguard++;
                        if (whiteMoves.Count > 5 && safeguard == 5)
                            continue;
                        processor.DrawImage(new GraphicsOptions(), image, new Point(660, 65 + safeguard * 21));
                        
                        processor.DrawText($"{file.ToString() + rank}", font.CreateFont(12, FontStyle.Bold), Rgba32.Black, new PointF(672, 63 + safeguard * 21));
                    }
                    for (var i = 0; i < whiteMoves.Count && i < 5; i++)
                    {
                        var player = whiteMoves[i].Player.ToString().ToLower();
                        var file = whiteMoves[i].Move.NewPosition.File;
                        var rank = whiteMoves[i].Move.NewPosition.Rank;
                        var fenChar = whiteMoves[i].MovedfenChar;
                        var image = Image.Load(_assetService.GetImagePath("Chess", $"{player}_{fenChar}.png"));
                        image.Mutate(img => img.Resize(new Size(12, 12)));
                        processor.DrawImage(new GraphicsOptions(), image, new Point(606, 65 + i * 21));

                        processor.DrawText($"{file.ToString() + rank}", font.CreateFont(12, FontStyle.Bold), Rgba32.Black, new PointF(618, 63 + i * 21));

                    }

                    #endregion
                });
                board.Save($"{Directory.GetCurrentDirectory()}\\Chessboards\\board{match.Id}-{match.HistoryList.Count}.png");
            });

            var nextPlayer = WhoseTurn(new ChessMatchModel
            {
                HistoryList = match.HistoryList,
                ChallengeeId = match.ChallengeeId,
                ChallengerId = match.ChallengerId
            });
            return new ImageLinkModel
            {
                ImageLink = $"attachment://board{match.Id}-{match.HistoryList.Count}.png",
                DirectoryLink = $"{Directory.GetCurrentDirectory()}\\Chessboards\\board{match.Id}-{match.HistoryList.Count}.png",
                NextPlayer = nextPlayer
            };
        }

        private async Task removeChallengeAsync(ChessChallengeModel challenge, Action<ChessChallengeModel> onTimeout)
        {
            while (challenge.TimeoutDate > DateTime.Now)
                await Task.Delay(250);
            challenge = _chessHandler.GetChallenge(challenge.Id);
            if (challenge.Accepted)
                return;
            await _chessHandler.RemoveChallengeAsync(challenge);
            onTimeout?.Invoke(challenge);
        }
        #endregion
    }
}