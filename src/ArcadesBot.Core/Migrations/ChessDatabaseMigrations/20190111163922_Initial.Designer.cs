﻿// <auto-generated />
using System;
using ArcadesBot.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ArcadesBot.Migrations
{
    [DbContext(typeof(ChessDatabase))]
    [Migration("20190111163922_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0-preview.18620.3");

            modelBuilder.Entity("ArcadesBot.Models.Chess.ChessChallengeModel", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Accepted");

                    b.Property<ulong>("ChallengeeId");

                    b.Property<ulong>("ChallengerId");

                    b.Property<ulong>("ChannelId");

                    b.Property<DateTime>("DateCreated");

                    b.Property<ulong>("GuildId");

                    b.Property<DateTime>("TimeoutDate");

                    b.HasKey("Id");

                    b.ToTable("ChessChallenge");
                });

            modelBuilder.Entity("ArcadesBot.Models.Chess.ChessMatchModel", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BlackAvatarUrl");

                    b.Property<ulong>("ChallengeeId");

                    b.Property<ulong>("ChallengerId");

                    b.Property<ulong>("ChannelId");

                    b.Property<ulong>("GuildId");

                    b.Property<string>("History");

                    b.Property<ulong>("NextPlayerId");

                    b.Property<ulong?>("StatId");

                    b.Property<int>("Status");

                    b.Property<string>("WhiteAvatarUrl");

                    b.Property<ulong>("Winner");

                    b.HasKey("Id");

                    b.HasIndex("StatId");

                    b.ToTable("ChessMatch");
                });

            modelBuilder.Entity("ArcadesBot.Models.Chess.ChessMatchStatsModel", b =>
                {
                    b.Property<ulong>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<ulong>("CreatedBy");

                    b.Property<int>("EndBy");

                    b.Property<DateTime>("EndDate");

                    b.Property<ulong>("GuildId");

                    b.Property<ulong>("MoveCount");

                    b.Property<string>("Participants");

                    b.Property<ulong>("Winner");

                    b.HasKey("Id");

                    b.ToTable("ChessMatchStatsModel");
                });

            modelBuilder.Entity("ArcadesBot.Models.Chess.ChessMatchModel", b =>
                {
                    b.HasOne("ArcadesBot.Models.Chess.ChessMatchStatsModel", "Stat")
                        .WithMany()
                        .HasForeignKey("StatId");
                });
#pragma warning restore 612, 618
        }
    }
}
