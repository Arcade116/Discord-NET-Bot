﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ArcadesBot.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ChessChallenge",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GuildId = table.Column<ulong>(nullable: false),
                    ChannelId = table.Column<ulong>(nullable: false),
                    ChallengerId = table.Column<ulong>(nullable: false),
                    ChallengeeId = table.Column<ulong>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    TimeoutDate = table.Column<DateTime>(nullable: false),
                    Accepted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChessChallenge", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChessMatchStatsModel",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GuildId = table.Column<ulong>(nullable: false),
                    Participants = table.Column<string>(nullable: true),
                    Winner = table.Column<ulong>(nullable: false),
                    EndBy = table.Column<int>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    MoveCount = table.Column<ulong>(nullable: false),
                    CreatedBy = table.Column<ulong>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChessMatchStatsModel", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ChessMatch",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    GuildId = table.Column<ulong>(nullable: false),
                    ChannelId = table.Column<ulong>(nullable: false),
                    ChallengerId = table.Column<ulong>(nullable: false),
                    ChallengeeId = table.Column<ulong>(nullable: false),
                    NextPlayerId = table.Column<ulong>(nullable: false),
                    History = table.Column<string>(nullable: true),
                    WhiteAvatarUrl = table.Column<string>(nullable: true),
                    BlackAvatarUrl = table.Column<string>(nullable: true),
                    Winner = table.Column<ulong>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    StatId = table.Column<ulong>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChessMatch", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChessMatch_ChessMatchStatsModel_StatId",
                        column: x => x.StatId,
                        principalTable: "ChessMatchStatsModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChessMatch_StatId",
                table: "ChessMatch",
                column: "StatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ChessChallenge");

            migrationBuilder.DropTable(
                name: "ChessMatch");

            migrationBuilder.DropTable(
                name: "ChessMatchStatsModel");
        }
    }
}
