﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArcadesBot.Migrations.GuildDatabaseMigrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ModWrapper",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    JoinRole = table.Column<ulong>(nullable: false),
                    MuteRole = table.Column<ulong>(nullable: false),
                    TextChannel = table.Column<ulong>(nullable: false),
                    LogDeletedMessages = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModWrapper", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WebhookWrapper",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Enabled = table.Column<bool>(nullable: false),
                    TextChannel = table.Column<ulong>(nullable: false),
                    WebhookId = table.Column<ulong>(nullable: false),
                    WebhookToken = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebhookWrapper", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Guilds",
                columns: table => new
                {
                    Id = table.Column<ulong>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Prefix = table.Column<string>(maxLength: 10, nullable: false),
                    IsConfigured = table.Column<bool>(nullable: false),
                    JoinMessages = table.Column<string>(nullable: true),
                    LeaveMessages = table.Column<string>(nullable: true),
                    ModId = table.Column<ulong>(nullable: true),
                    AssignableRoles = table.Column<string>(nullable: true),
                    Afk = table.Column<string>(nullable: true),
                    BlackListedChannels = table.Column<string>(nullable: true),
                    WebhookId = table.Column<ulong>(nullable: true),
                    Profiles = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Guilds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Guilds_ModWrapper_ModId",
                        column: x => x.ModId,
                        principalTable: "ModWrapper",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Guilds_WebhookWrapper_WebhookId",
                        column: x => x.WebhookId,
                        principalTable: "WebhookWrapper",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Guilds_ModId",
                table: "Guilds",
                column: "ModId");

            migrationBuilder.CreateIndex(
                name: "IX_Guilds_WebhookId",
                table: "Guilds",
                column: "WebhookId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Guilds");

            migrationBuilder.DropTable(
                name: "ModWrapper");

            migrationBuilder.DropTable(
                name: "WebhookWrapper");
        }
    }
}
