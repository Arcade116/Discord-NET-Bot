﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArcadesBot.Migrations.GuildDatabaseMigrations
{
    public partial class V11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "LastEditedbyId",
                table: "Guilds",
                nullable: false,
                defaultValue: 0ul);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastEditedbyId",
                table: "Guilds");
        }
    }
}
