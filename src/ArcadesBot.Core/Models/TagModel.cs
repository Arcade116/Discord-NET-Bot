﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ArcadesBot.Models
{
    public class TagModel
    {
        public string TagName { get; set; }
        public ulong OwnerId { get; set; }
        [NotMapped]
        public List<string> AliassesList { get; set; } = new List<string>();

        public string Aliasses
        {
            get => JsonConvert.SerializeObject(AliassesList);
            set => AliassesList = JsonConvert.DeserializeObject<List<string>>(value);
        }
        public ulong Uses { get; set; } = 0;
        public string Content { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
    }
}