﻿using System.Collections.Generic;

namespace ArcadesBot.Models
{
    public class ConfigModel
    {
        public string Prefix { get; set; } = "%";
        public List<string> Games { get; set; } = new List<string>();
        public List<ulong> Blacklist { get; set; } = new List<ulong>();
        public Dictionary<string, string> ApiKeys { get; set; }
    }
}