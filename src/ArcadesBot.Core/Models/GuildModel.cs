﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Newtonsoft.Json;

namespace ArcadesBot.Models
{
    public class GuildModel : IEntity<ulong>
    {
        [DisplayName("Guild's Id")]
        [Required]
        public ulong Id { get; set; }
        [DisplayName("Command Prefix")]
        [Required(ErrorMessage = "Prefix is required")]
        [MinLength(1), MaxLength(10, ErrorMessage = "Prefix cannot be longer than 10 characters")]
        public string Prefix { get; set; }
        public bool IsConfigured { get; set; }
        [DisplayName("Join Messages")]
        [NotMapped]
        public List<string> JoinMessagesList { get; set; } = new List<string>(5);
        public string JoinMessages
        {
            get => JsonConvert.SerializeObject(JoinMessagesList);
            set => JoinMessagesList = JsonConvert.DeserializeObject<List<string>>(value);
        }
        [DisplayName("Leave Messages")]
        [NotMapped]
        public List<string> LeaveMessagesList { get; set; } = new List<string>(5);
        public string LeaveMessages
        {
            get => JsonConvert.SerializeObject(LeaveMessagesList);
            set => LeaveMessagesList = JsonConvert.DeserializeObject<List<string>>(value);
        }

        public ModWrapper Mod { get; set; } = new ModWrapper();

        [DisplayName("Assignable Roles")]
        [NotMapped]
        public List<ulong> AssignableRolesList { get; set; } = new List<ulong>(10);
        public string AssignableRoles
        {
            get => JsonConvert.SerializeObject(AssignableRolesList);
            set => AssignableRolesList = JsonConvert.DeserializeObject<List<ulong>>(value);
        }

        [NotMapped]
        public Dictionary<ulong, string> AfkDict { get; set; } = new Dictionary<ulong, string>();
        public string Afk
        {
            get => JsonConvert.SerializeObject(AfkDict);
            set => AfkDict = JsonConvert.DeserializeObject<Dictionary<ulong, string>>(value);
        }
        [DisplayName("Blacklisted Channels")]
        [NotMapped]
        public List<ulong> BlackListedChannelsList { get; set; } = new List<ulong>();
        public string BlackListedChannels
        {
            get => JsonConvert.SerializeObject(BlackListedChannelsList);
            set => BlackListedChannelsList = JsonConvert.DeserializeObject<List<ulong>>(value);
        }

        public WebhookWrapper Webhook { get; set; }
        [NotMapped]
        public Dictionary<ulong, UserProfile> ProfilesDict { get; set; } = new Dictionary<ulong, UserProfile>();
        public string Profiles
        {
            get => JsonConvert.SerializeObject(ProfilesDict);
            set => ProfilesDict = JsonConvert.DeserializeObject<Dictionary<ulong, UserProfile>>(value);
        }

        [NotMapped]
        public List<TagModel> TagList { get; set; } = new List<TagModel>();

        public string Tags
        {
            get => JsonConvert.SerializeObject(TagList);
            set => TagList = JsonConvert.DeserializeObject<List<TagModel>>(value);
        }

        public ulong LastEditedbyId { get; set; }

        public GuildModel() { }

        public GuildModel(ulong guildId)
        {
            Id = guildId;
            Prefix = JsonConvert.DeserializeObject<ConfigModel>(File.ReadAllText($"{Directory.GetCurrentDirectory()}/config/config.json")).Prefix;
        }
    }
}