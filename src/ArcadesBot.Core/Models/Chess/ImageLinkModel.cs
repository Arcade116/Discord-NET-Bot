﻿namespace ArcadesBot.Models.Chess
{
    public class ImageLinkModel
    {
        public string ImageLink { get; set; }
        public string DirectoryLink { get; set; }
        public ulong NextPlayer { get; set; }
        public bool IsCheck { get; set; } = false;
        public Cause Status { get; set; } = Cause.OnGoing;
        public ulong Winner { get; set; } = 0;
        public bool IsCheckmated { get; set; } = false;
        public bool IsStalemated { get; set; } = false;
    }
}