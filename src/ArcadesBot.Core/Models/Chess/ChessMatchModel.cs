﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ArcadesBot.Models.Chess
{
    public class ChessMatchModel : IEntity<ulong>
    {
        public ulong Id { get; set; }
        public ulong GuildId { get; set; }
        public ulong ChannelId { get; set; }
        public ulong ChallengerId { get; set; }
        public ulong ChallengeeId { get; set; }
        public ulong NextPlayerId { get; set; }
        [NotMapped]
        public List<ChessMoveModel> HistoryList { get; set; }
        public string History
        {
            get => JsonConvert.SerializeObject(HistoryList);
            set => HistoryList = JsonConvert.DeserializeObject<List<ChessMoveModel>>(value);
        }
        public string WhiteAvatarUrl { get; set; }
        public string BlackAvatarUrl { get; set; }
        public ulong Winner { get; set; } = 1;
        public Cause Status { get; set; } = Cause.OnGoing;
        public ChessMatchStatsModel Stat { get; set; }
    }
}