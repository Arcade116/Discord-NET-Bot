﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using Newtonsoft.Json;

namespace ArcadesBot.Models.Chess
{
    public class ChessMatchStatsModel : IEntity<ulong>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public ulong Id { get; set; }
        public ulong GuildId { get; set; }
        [NotMapped]
        public ulong[] ParticipantsArray { get; set; } = new ulong[2];

        public string Participants
        {
            get => JsonConvert.SerializeObject(ParticipantsArray);
            set => ParticipantsArray = JsonConvert.DeserializeObject<ulong[]>(value);
        }
        public ulong Winner { get; set; }
        public Cause EndBy { get; set; }
        public DateTime EndDate { get; set; } = DateTime.Now;
        public ulong MoveCount { get; set; }
        public ulong CreatedBy { get; set; }
    }

    public enum Cause
    {
        OnGoing,
        Resign,
        Stalemate,
        Checkmate
    }
}