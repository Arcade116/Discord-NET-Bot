﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ArcadesBot.Models.Chess
{
    public class ChessChallengeModel : IEntity<ulong>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public ulong Id { get; set; }
        public ulong GuildId { get; set; }
        public ulong ChannelId { get; set; }
        public ulong ChallengerId { get; set; }
        public ulong ChallengeeId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime TimeoutDate { get; set; }
        public bool Accepted { get; set; } = false;
    }
}